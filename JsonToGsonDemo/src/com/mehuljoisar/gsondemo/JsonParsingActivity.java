package com.mehuljoisar.gsondemo;

import java.io.IOException;
import java.io.InputStream;
import java.util.List;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.HttpStatus;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;

import android.app.Activity;
import android.os.Bundle;
import android.util.Log;

import com.google.gson.Gson;
import com.mehuljoisar.gsondemo.model.Country;
import com.mehuljoisar.gsondemo.model.ResultPopulation;

public class JsonParsingActivity extends Activity {
	
	private String url = "http://www.androidbegin.com/tutorial/jsonparsetutorial.txt";
	private final String TAG = "JsonParsingActivity";
	
    @Override
    public void onCreate(Bundle savedInstanceState) {
    	
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main);
        
        Gson gson = new Gson();
        String jsonResponse  = WebAPIRequest.performGet(url);
        
        Log.e(TAG, jsonResponse);
        ResultPopulation response = gson.fromJson(jsonResponse, ResultPopulation.class);
        
        List<Country> results = response.getWorldpopulation();
        
        for (Country mCountry : results) {
        	Log.e(TAG, mCountry.toString());
		}
    }
}